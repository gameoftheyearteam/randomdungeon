﻿using System.Collections;
using UnityEngine;

public class EnemyArcher : MonoBehaviour
{
    [SerializeField] ProjectileController arrowPrefab;
    [SerializeField] float minDistance;
    [SerializeField] float maxDistance;
    [SerializeField] float timeToAttack;
    [SerializeField] float attackDamage;
    [SerializeField] float movementSpeed;
    [SerializeField] float projectileSpeed;

    PlayerController player;

    bool isAttacking = false;


    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }


    void Update()
    {
        if (!isAttacking)
        {
            if (Vector3.Distance(transform.position, player.transform.position) < minDistance)
            {
                Vector3 temp = transform.position - player.transform.position;
                transform.position = Vector3.MoveTowards(transform.position, transform.position + temp, movementSpeed * Time.deltaTime);
            }
            else if (Vector3.Distance(transform.position, player.transform.position) > maxDistance)
            {
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, movementSpeed * Time.deltaTime);
            }
            else
            {
                StartCoroutine(Shoot());
            }
        }
    }


    IEnumerator Shoot()
    {
        isAttacking = true;

        //Animation;
        yield return new WaitForSeconds(timeToAttack);

        ProjectileController arrow = Instantiate(arrowPrefab);
        arrow.transform.position = transform.position;
        arrow.transform.LookAt(player.transform.position);

        arrow.Speed = projectileSpeed;
        arrow.Damage = attackDamage;
        arrow.TargetPosition = player.transform.position;
        arrow.TravelDirection = player.transform.position - transform.position;
        arrow.IsByEnemy = true;

        isAttacking = false;
    }
}
