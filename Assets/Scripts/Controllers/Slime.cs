﻿using UnityEngine;

public class Slime : MonoBehaviour
{
    [SerializeField] float movementSpeed = 0.5f;

    PlayerController player;


    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }


    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, movementSpeed * Time.deltaTime);
    }
}
