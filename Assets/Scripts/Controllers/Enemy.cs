﻿using System;
using UnityEngine;
using DG.Tweening;

public class Enemy : MonoBehaviour
{
    public event Action<float> OnDeath;


    [SerializeField] float health;
    [SerializeField] float exp;
    [SerializeField] float hitDamage;
    [SerializeField] float knockbackPower;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController player = other.GetComponent<PlayerController>();

            player.TakeDamage(hitDamage);

            Vector3 temp = transform.position - player.transform.position;

            transform.DOLocalMove(transform.position + temp * knockbackPower, 1);
        }
    }


    public void TakeDamage(float damage)
    {
        health -= damage;

        Debug.Log("Damage taken " + damage);

        if (health <= 0)
        {
            gameObject.SetActive(false);
            OnDeath(exp);
            Destroy(gameObject);
        } 
    }
}
