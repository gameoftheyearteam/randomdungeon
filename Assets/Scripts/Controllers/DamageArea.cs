﻿using UnityEngine;

public class DamageArea : MonoBehaviour
{
    public float Damage { get; set; }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<Enemy>().TakeDamage(Damage);
        }
    }
}
