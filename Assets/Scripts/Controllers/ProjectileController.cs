﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour
{
    [SerializeField] float disappearTime = 4f;
    [SerializeField] float disappearAfterHitTime;

    bool isMoving = true;


    public Vector3 TargetPosition { get; set; }
    public Vector3 TravelDirection { get; set; }
    public float Speed { get; set; }
    public float Damage { get; set; }
    public bool IsByEnemy { get; set; }


    void Start()
    {
        StartCoroutine(Disappear());
    }


    void Update()
    {
        if (isMoving)
        {
            if (transform.position == TargetPosition)
            {
                TargetPosition = transform.position + TravelDirection;
            }
            transform.position =  Vector3.MoveTowards(transform.position, TargetPosition, Speed * Time.deltaTime);
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            if (!IsByEnemy)
            {
                other.GetComponent<Enemy>().TakeDamage(Damage);
                Destroy(gameObject);
            }
        }
        else if (other.tag == "Obstacle")
        {
            isMoving = false;
            StartCoroutine(DisappearAfterHit());
        }
        else if (other.tag == "Player")
        {
            if (IsByEnemy)
            {
                other.GetComponent<PlayerController>().TakeDamage(Damage);
                Destroy(gameObject);
            }
        }
    }


    IEnumerator DisappearAfterHit()
    {
        yield return new WaitForSeconds(disappearAfterHitTime);
        Destroy(gameObject);
    }


    IEnumerator Disappear()
    {
        yield return new WaitForSeconds(disappearTime);
        Destroy(gameObject);
    }
}
