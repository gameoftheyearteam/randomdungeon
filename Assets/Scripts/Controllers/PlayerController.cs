﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    #region Fields

    [SerializeField] DamageArea damageAreaPrefab;
    [SerializeField] ProjectileController arrowPrefab;
    [SerializeField] PlayerStats[] races;
    [SerializeField] WeaponStats[] weapons;

    float health;
    float currentHealth;
    float movementSpeed;
    float meleeDamageMultiplier;
    float rangeDamageMultiplier;
    float baseDamage;
    float attacksPerSecond;
    float timeToAttack;
    float attackRange;
    float projectileSpeed;
    WeaponType currentWeaponType;

    PlayerStats playerStats;
    WeaponStats weaponStats;
    DamageArea damageArea;
    Transform target;
    float timeBeforeAttack;
    bool isMoving = false;
    bool isAttacking = false;
    bool isShooting = false;

    #endregion



    #region Properties

    float Damage
    {
        get
        {
            if (currentWeaponType == WeaponType.Melee)
            {
                return baseDamage * meleeDamageMultiplier;
            }
            else if (currentWeaponType == WeaponType.Range)
            {
                return baseDamage * rangeDamageMultiplier;
            }
            else
            {
                return baseDamage;
            }
        }
    }

    #endregion



    #region Unity Lifecycle

    void OnEnable()
    {
        InputManager.Instance.OnMovement += Move;
        InputManager.Instance.OnStop += FindTarget;
    }


    void Start()
    {
        damageArea = Instantiate(damageAreaPrefab, transform);
        damageArea.gameObject.SetActive(false);
    }
    

    void Update()
    {
        if (!isMoving)
        {
            if (currentWeaponType == WeaponType.Melee)
            {
                if (timeBeforeAttack <= 0f)
                {
                    StartCoroutine(Attack());
                }
            }
            else if (currentWeaponType == WeaponType.Range)
            {
                if (!isShooting)
                {
                    if ((target != null) && (Vector3.Distance(target.position, transform.position) < attackRange))
                    {
                        isShooting = true;

                        timeBeforeAttack = 1f / attacksPerSecond;

                        //Animation
                    }
                }
                else
                {
                    if (timeBeforeAttack <= 0f)
                    {
                        if ((target != null) && (Vector3.Distance(target.position, transform.position) < attackRange))
                        {
                            Shoot();
                        }
                    }
                }
            }
        }

        if (timeBeforeAttack > 0f)
        {
            timeBeforeAttack -= Time.deltaTime;
        }

        isMoving = false;
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);

        if (target != null)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(target.position, target.localScale.x);
        }
    }


    void OnDisable()
    {
        InputManager.Instance.OnMovement -= Move;
        InputManager.Instance.OnStop -= FindTarget;
    }

    #endregion



    #region Public Methods

    public void SetPlayerStats(WeaponType weaponType)
    {
        if (weaponType == WeaponType.Melee)
        {
            playerStats = races[0];
        }
        else
        {
            playerStats = races[1];
        }

        float[] stats = playerStats.GetStats();

        health = stats[0];
        currentHealth = health;
        UIManager.Instance.SetHPTo(1);

        movementSpeed = stats[1];
        meleeDamageMultiplier = stats[2];
        rangeDamageMultiplier = stats[3];
    }


    public void RefreshWeapon(WeaponType weaponType)
    {
        if (weaponType == WeaponType.Melee)
        {
            weaponStats = weapons[0];
        }
        else
        {
            weaponStats = weapons[1];
        }

        currentWeaponType = weaponStats.GetWeaponType();
        float[] stats = weaponStats.GetStats();

        baseDamage = stats[0];
        attacksPerSecond = stats[1];
        timeToAttack = stats[2];
        attackRange = stats[3];
        projectileSpeed = stats[4];

        if (timeToAttack == 0)
        {
            timeToAttack = 1f / attacksPerSecond;
        }
    }


    public void FindTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        GameObject nearestEnemy = null;
        float shortestDistance = Mathf.Infinity;

        foreach (GameObject enemy in enemies)
        {
            float distance = Vector3.Distance(transform.position, enemy.transform.position);

            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                nearestEnemy = enemy;
            }
        }

        if ((nearestEnemy != null) && (shortestDistance <= attackRange))
        {
            target = nearestEnemy.transform;
            transform.LookAt(target);
        }
        else
        {
            target = null;
        }
    }


    public void TakeDamage(float Damage)
    {
        currentHealth -= Damage;

        UIManager.Instance.SetHPTo(currentHealth / health);

        if (currentHealth <= 0)
        {
            InputManager.Instance.IsEnabled = false;
            UIManager.Instance.ShowDeathScreen();

            Time.timeScale = 0;
        }
    }

    #endregion



    #region PrivateMethods

    void Move(Vector3 dir)
    {
        if (!isAttacking)
        {
            if (currentWeaponType == WeaponType.Range)
            {
                timeBeforeAttack = 0f;
                isShooting = false;
            }

            isMoving = true;

            Vector3 temp = new Vector3(dir.x, 0f, dir.y) * .01f;
            transform.LookAt(temp + transform.position, Vector3.up);

            transform.position = Vector3.MoveTowards(transform.position, transform.position + temp, Time.deltaTime * movementSpeed);
        }
    }


    IEnumerator Attack()
    {
        if ((target != null) && (Vector3.Distance(target.position, transform.position) < attackRange))
        {
            isAttacking = true;
            damageArea.gameObject.SetActive(true);
                
            damageArea.transform.localScale = 
                new Vector3(attackRange * 2f, damageAreaPrefab.transform.localScale.y, attackRange * 2f);
            damageArea.Damage = Damage;

            timeBeforeAttack = 1f / attacksPerSecond;

            //Animation
            yield return new WaitForSeconds(timeToAttack);

            damageArea.gameObject.SetActive(false);
            //Destroy(damageArea.gameObject);

            isAttacking = false;
        }
    }


    void Shoot()
    {
        ProjectileController arrow = Instantiate(arrowPrefab);
        arrow.transform.position = transform.position;
        arrow.transform.LookAt(target.position);

        arrow.Speed = projectileSpeed;
        arrow.Damage = Damage;
        arrow.TargetPosition = target.position;
        arrow.TravelDirection = target.position - transform.position;
        arrow.IsByEnemy = false;

        isShooting = false;
    }
    
    #endregion
}
