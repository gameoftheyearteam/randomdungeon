﻿using UnityEngine;
using System;

public class InputManager : SingletonMonoBehaviour<InputManager>
{
    public event Action<Vector3> OnMovement;
    public event Action OnStop;


    #region Fields

    Vector3 startPosition;
    Vector3 endPosition;

    #endregion



    #region Properties

    public bool IsEnabled { get; set; }

    #endregion



    #region Unity Lifecycle

    void Start()
    {
        IsEnabled = true;
    }


    void Update()
    {
        if (IsEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                startPosition = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                endPosition = Input.mousePosition;
                OnMovement?.Invoke(endPosition - startPosition);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                OnStop?.Invoke();
            }
        }
    }

    #endregion
}