﻿using UnityEngine;
using Cinemachine;
using System.Collections.Generic;

public class LevelManager : SingletonMonoBehaviour<LevelManager>
{
    #region Fields

    [SerializeField] Camera mainCameraPrefab;
    [SerializeField] Light mainLightPrefab;
    [SerializeField] PlayerController playerPrefab;
    [SerializeField] List<LevelTheme> levelThemes;

    Camera mainCamera;
    Light mainDirLight;
    PlayerController player;

    LevelTheme currentTheme;
    GameObject currentLevel;
    Enemy[] enemies;
    Exit exit;

    int currentLevelIndex;
    float levelExp;

    #endregion



    #region Unity Lifecycle

    protected override void Awake()
    {
        base.Awake();

        mainCamera = Instantiate(mainCameraPrefab, transform);
        mainDirLight = Instantiate(mainLightPrefab, transform);

        
    }


    void Start()
    {
        UIManager.Instance.CanvasCamera = mainCamera;
    }

    #endregion



    #region Public Methods

    public void StartGame(WeaponType weaponType)
    {
        if (currentLevel != null)
        {
            Destroy(currentLevel.gameObject);

            player.transform.position = new Vector3(0f, player.transform.localScale.y / 2, 0f);
        }

        currentTheme = levelThemes[0];
        currentLevel = currentTheme.GetFirst();
        currentLevelIndex = 0;

        LoadLevel();

        if (player == null)
        {
            player = Instantiate(playerPrefab, transform);

            SetCamera();
        }

        player.SetPlayerStats(weaponType);
        player.RefreshWeapon(weaponType);
    }


    public void NextLevel()
    {
        if (currentLevelIndex + 1 < currentTheme.Count)
        {
            Destroy(currentLevel.gameObject);

            currentLevelIndex++;
            currentLevel = currentTheme.Get(currentLevelIndex);

            LoadLevel();

            player.transform.position = new Vector3(0f, player.transform.localScale.y / 2, 0f);
        }
        else
        {
            UIManager.Instance.ShowDeathScreen();
            InputManager.Instance.IsEnabled = false;
        }
    }


    public void LoadLevel()
    {
        currentLevel = Instantiate(currentLevel, transform);

        exit = FindObjectOfType<Exit>();
        exit.gameObject.SetActive(false);

        enemies = FindObjectsOfType<Enemy>();

        foreach (Enemy enemy in enemies)
        {
            enemy.OnDeath += ProcessDeath;
        }

        levelExp = 0f;

        InputManager.Instance.IsEnabled = true;
    }


    public void ProcessDeath(float exp)
    {
        levelExp += exp;

        player.FindTarget();

        enemies = FindObjectsOfType<Enemy>();
        
        if (enemies.Length == 0)
        {
            exit.gameObject.SetActive(true);
        }
    }

    #endregion 



    #region Private Methods

    void SetCamera()
    {
        CinemachineVirtualCamera virtualCamera =
                LevelManager.Instance.mainCamera.GetComponentInChildren<CinemachineVirtualCamera>();
        virtualCamera.Follow = player.transform;
    }

    #endregion
}
