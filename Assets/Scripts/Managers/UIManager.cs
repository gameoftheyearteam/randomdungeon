﻿using UnityEngine;

public class UIManager : SingletonMonoBehaviour<UIManager>
{
    [SerializeField] Canvas canvas;
    [SerializeField] GameScreen gameScreenPrefab;
    [SerializeField] MainMenuScreen mainMenuScreenPrefab;
    [SerializeField] DeathScreen deathScreenPrefab;

    Transform canvasTransform;
    GameScreen gameScreen;
    DeathScreen deathScreen;
    MainMenuScreen mainMenuScreen;


    public Camera CanvasCamera
    {
        get
        {
            return canvas.worldCamera;
        }
        set
        {
            canvas.worldCamera = value;
        }
    }


    protected override void Awake()
    {
        base.Awake();

        canvasTransform = canvas.transform;
        canvas.planeDistance = 1f;

        mainMenuScreen = Instantiate(mainMenuScreenPrefab, canvasTransform);

        gameScreen = Instantiate(gameScreenPrefab, canvasTransform);

        deathScreen = Instantiate(deathScreenPrefab, canvasTransform);
    }


    public void SetHPTo(float value)
    {
        gameScreen.SetHPTo(value);
    }


    public void HideGameScreen()
    {
        gameScreen.gameObject.SetActive(false);
    }


    public void StartGameAsMelee()
    {
        LevelManager.Instance.StartGame(WeaponType.Melee);
        gameScreen.Show();
        Time.timeScale = 1;
    }


    public void StartGameAsRange()
    {
        LevelManager.Instance.StartGame(WeaponType.Range);
        gameScreen.Show();
        Time.timeScale = 1;
    }


    public void ShowDeathScreen()
    {
        deathScreen.gameObject.SetActive(true);
    }


    public void HideDeathScreen()
    {
        gameScreen.Hide();
        deathScreen.gameObject.SetActive(false);
    }


    public void ShowMainMenuScreen()
    {
        mainMenuScreen.gameObject.SetActive(true);
    }


    public void HideMainMenuScreen()
    {
        mainMenuScreen.gameObject.SetActive(false);
    }
}
