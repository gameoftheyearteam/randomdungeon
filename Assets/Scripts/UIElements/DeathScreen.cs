﻿using UnityEngine;
using UnityEngine.UI;

public class DeathScreen : MonoBehaviour
{
    [SerializeField] Button buttonRestart;


    void OnEnable()
    {
        buttonRestart.onClick.AddListener(UIManager.Instance.ShowMainMenuScreen);
        buttonRestart.onClick.AddListener(Hide);
        buttonRestart.onClick.AddListener(UIManager.Instance.HideGameScreen);
    }


    public void Show()
    {
        gameObject.SetActive(true);
    }


    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
