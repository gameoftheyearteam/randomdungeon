﻿using UnityEngine;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour
{
    [SerializeField] Image bar;


    public void Show()
    {
        gameObject.SetActive(true);
    }


    public void Hide()
    {
        gameObject.SetActive(false);
    }


    public void SetHPTo(float value)
    {
        if (value < 0)
        {
            value = 0;
        }

        Vector3 temp = bar.rectTransform.localScale;
        bar.rectTransform.localScale = new Vector3(value, temp.y, temp.z);
    }
}
