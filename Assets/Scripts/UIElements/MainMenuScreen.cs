﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuScreen : MonoBehaviour
{
    [SerializeField] Button buttonMelee;
    [SerializeField] Button buttonRange;


    void OnEnable()
    {
        buttonMelee.onClick.AddListener(UIManager.Instance.StartGameAsMelee);
        buttonMelee.onClick.AddListener(Hide);

        buttonRange.onClick.AddListener(UIManager.Instance.StartGameAsRange);
        buttonRange.onClick.AddListener(Hide);
    }


    public void Show()
    {
        gameObject.SetActive(true);
    }


    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
