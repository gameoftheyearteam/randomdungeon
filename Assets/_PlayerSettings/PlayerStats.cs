﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewPlayerStats", menuName = "PlayerStats")]
public class PlayerStats : ScriptableObject
{
    [SerializeField] float health;
    [SerializeField] float movementSpeed;
    [SerializeField] float meleeDamageMultiplier;
    [SerializeField] float rangeDamageMultiplier;


    public float[] GetStats()
    {
        return new float[] { health, movementSpeed, meleeDamageMultiplier, rangeDamageMultiplier };
    }
}
