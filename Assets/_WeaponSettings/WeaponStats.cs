﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "Weapon")]
public class WeaponStats : ScriptableObject
{
    [SerializeField] WeaponType type;

    [SerializeField] float damage;
    [SerializeField] float attacksPerSecond;
    [SerializeField] float timeToAttack;
    [SerializeField] float attackRange;

    [SerializeField] float projectileSpeed;


    public WeaponType GetWeaponType()
    {
        return type;
    }


    public float[] GetStats()
    {
        return new float[] { damage, attacksPerSecond, timeToAttack, attackRange, projectileSpeed };
    }
}
