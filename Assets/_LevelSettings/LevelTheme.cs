﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTheme", menuName = "LevelTheme")]
public class LevelTheme : ScriptableObject
{
    [SerializeField] Material darkPlatform;
    [SerializeField] Material lightPlatform;
    [SerializeField] List<GameObject> levels;
    [SerializeField] List<GameObject> bosses;


    public int Count
    {
        get
        {
            return levels.Count;
        }
    }


    public GameObject GetFirst()
    {
        return levels[0];
    }


    public GameObject GetRandom()
    {
        return levels[Random.Range(0, levels.Count - 1)];
    }


    public GameObject Get(int index)
    {
        return levels[index];
    }
}
